# Práctica final correspondiente al curso de JavaScript 
## Cabrera Pérez Oswaldo

**Solución**

La primera parte fue hacer la llamada a la API concatenando la palabra buscada

    url: "https://api.themoviedb.org/3/search/movie?query="+ palabra +"&api_key=feb6f0eeaa0a72662967d77079850353&certification_country=MX&language=es"

Una vez que la respuesta fue exitosa, utilizamos nuestra variable *miLista* para comenzar a guardar los resultados de la siguiente manera:

    cardHTML = crearMovieCard(elemento); 
	miLista.append(cardHTML);

Reutilicé la función crearMovieCard solo agregando el acceso al atributo *overview* para obtener la descripción de las pelicula. En total utilicé los atributos *original_title, poster.path y overview*

### Resultado

![prueba](images/prueba.png)