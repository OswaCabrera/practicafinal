$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		var miLista = $("#loader");

		//console.console.log("https://api.themoviedb.org/3/search/movie?query="+ palabra +"&api_key=feb6f0eeaa0a72662967d77079850353");

		$.ajax({

			//Por alguna razon esta llamada no funcionaba
			//url: "http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=1231231231564464541231&query="+palabra,
			
			//Encontre otra forma de hacerla la cual es la siguiente:
			url: "https://api.themoviedb.org/3/search/movie?query="+ palabra +"&api_key=feb6f0eeaa0a72662967d77079850353&certification_country=MX&language=es",
			success: function(respuesta) {
				console.log(respuesta);
				

				setTimeout(function () {
					miLista.empty();
					 
					 //Se elimina la imagen de "cargando" (los engranes)
	
					//Para cada elemento en la lista de resultados (para cada pelicula)
					$.each(respuesta.results, function(index, elemento) {
						//La función crearMovieCard regresa el HTML con la estructura de la pelicula
						cardHTML = crearMovieCard(elemento); 
						miLista.append(cardHTML);
					});
	
				}, 3000); //Tarda 3 segundos en ejecutar la función de callback
						  //Sino no se vería la imagen de los engranes, da al usuario la sensación de que se está obteniendo algo.
	
			},
			error: function() {
				console.log("No se ha podido obtener la información");
				$('#loader').remove();
				$('#miLista').html('No se ha podido obtener la información');
			},
			beforeSend: function() { 
				//ANTES de hacer la petición se muestra la imagen de cargando.
				miLista.empty();
				console.log('CARGANDO');
				//$('#loader').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
				miLista.append('<div class="text-center"><img src="images/loading.gif" /></div>');
			},
		});		

	});


	
});

function crearMovieCard(movie){
	//Llega el objeto JSON de UNA película, como la regresa la API
	console.log(movie.poster_path);
    //sabemos que el directorio donde se guardan las imágenes es: https://image.tmdb.org/t/p/w500/
    //el atributo movie.poster_path del objeto movie, sólo contiene el nombre de la imagen (NO la ruta completa)

    //NOTAR que se accede al objeto JSON movie con la notación de punto para acceder a los atributos (movie.original_title).
	var cardHTML =
		'<!-- CARD -->'
		+'<div class="col-md-4">'
		    +'<div class="card">'
		       +'<div class="card-header">'
		          +'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+movie.poster_path+'" alt="Card image">'
		       +'</div>'
		       +'<div class="card-body">'
		          +'<h2 class="card-title">'+movie.original_title+'</h2>'
		          +'<div class="container">'
		             +'<div class="row">'
		                /*+'<div class="col-4 metadata">'
		                   +'<i class="fa fa-star" aria-hidden="true"></i>'
		                   +'<p>9.5/10</p>'
		                +'</div>'*/
		                //+'<div class="col-8 metadata">Adventure</div>'
		             +'</div>'
		          +'</div>'
		          +'<p class="card-text">'+movie.overview+'</p>'
		       +'</div>'
		    +'</div>'
		+'</div>'
		+'<!-- CARD -->';

		return cardHTML;
}


